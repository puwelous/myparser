/**
Usage examples:
$ myparser -f subor.txt,subor2.txt,subor3.txt
$ myparser -dir adresarSoSubormi
$ myparser -e error
$ myparser -r	//scanning recursively, searching for all matches not one only
*/
#ifdef _WIN32
#define _CRT_SECURE_NO_DEPRECATE
#endif

#include <stdio.h>
#include <stdlib.h>

#define FILES_COUNT 512
#define DIRS_COUNT 512
#define LINE_SIZE 512

char *separator = "------------------------------------------\n";
char *DEFAULT_EXPRESSION = "exception";
char *searched_expression = NULL;

//argshandler.h
extern void printArgs(int argc, char* argv[]);
extern int isArgument(const char *argument, int argc, char *argv[]);
extern int checkArgsCountMinInc(int argc, int min);

//arrayPrinter.h
extern void printPrefixedArray(char *prefix, int size, char *fileNames[]);

//myDirHandler.h
extern int get_directory_file_names(char *dir,int filesRealCount, char *fileNames[]);

//check myStringHandler.c, myStringHandler.h
extern int split_string_according_separator(const char separator, char *handledString, const int suggestedFilesCount, char *fileNames[]);
extern const char *mystristr(const char *haystack, const char *needle);

//myMemoryHandler.h
extern void free1DArray(int filesCount, char *fileNames[]);
extern void null1DArray(int filesCount, char *fileNames[]);

void setSearchedExpression(int *p_argIndex, int argc, char* argv[]);
int parse_file_names(int indexOfF, char *argv[], char *fileNamesp[]);
int parse_dir_names(int indexOfF, char *argv[], char *dirNamesp[]);
void handle_files_once_only(int filesRealCount,char *fileNames[]);
void handle_files_recursively(int filesRealCount,char *fileNames[]);
void find_expression_once_only(char * name, FILE * fr);
void find_expression_recursively(char * name, FILE * fr);

/******************************************* MAIN *******************************************/
int main(int argc, char *argv[] ){
	int arg_index;
	char *fileNames[FILES_COUNT];
	char *argDirNames[DIRS_COUNT];
	int filesRealCount = 0;
	int dirsRealCount = 0;
	int isRecursiveScanRequired = 0;
	int freeMemoryForFileNamesRequired = 0;
			
	printf(separator);

	if(!checkArgsCountMinInc(argc,2)){
		printf("Not enough arguments(%d)\nExiting...\n", argc);
		printArgs(argc, argv);
		return 0;
	}

	isRecursiveScanRequired = isArgument("-r",argc,argv);

	setSearchedExpression( &arg_index, argc,argv);

	if( (arg_index = isArgument("-f",argc,argv)) > 0 )
	{
		printf("... direct files scan has been chosen ...\n");
		if( (filesRealCount = parse_file_names(arg_index, argv, fileNames))<1 )
		{
			printf("NonPositive amount of files could be parsed from an command line arguments!\n");
			exit(-1);
		}
	}else if( (arg_index = isArgument("-dir",argc,argv))>0 )
	{
		int i = 0;
		int previousFilesCount = 0;

		printf("... a directory scan has been chosen ...\n");
		if( (dirsRealCount = parse_dir_names(arg_index, argv, argDirNames))<1 )
		{
			printf("Negative amount of directories could be parsed from an command line arguments!\n");
			exit(-1);
		}

		printf("%d directory names parsed succesfully...\n",dirsRealCount);
		printPrefixedArray("Directories:\n",dirsRealCount, argDirNames);

		previousFilesCount = filesRealCount;
		for( i=0 ; i < dirsRealCount ; i++)
		{
			printf("... handling directory %s ...\n", argDirNames[i]);
			filesRealCount = get_directory_file_names(argDirNames[i], previousFilesCount, fileNames);
			if( (filesRealCount - previousFilesCount) <= 0)
			{
				fprintf(stderr," Directory %s is empty or error occured\n", argDirNames[i]);
			}else{
				printf("%d file names from directory %s\\ parsed succesfully\n", (filesRealCount - previousFilesCount), argDirNames[i]);
				previousFilesCount = filesRealCount;
			}
		}

		freeMemoryForFileNamesRequired = (filesRealCount > 0);
	}else{
		printf("Neither file(s) [-f], neither directories [-dir] has been defined via args!\nExiting...\n");
		exit(0);
	}
	
	printf("-- Summary: %d file names parsed succesfully... --\n",filesRealCount);
	printPrefixedArray("Files:\n", filesRealCount, fileNames);

	if(isRecursiveScanRequired)
		handle_files_recursively(filesRealCount, fileNames);
	else
		handle_files_once_only(filesRealCount, fileNames);

	if(freeMemoryForFileNamesRequired)
		free1DArray(filesRealCount, fileNames);
	else
		null1DArray(filesRealCount, fileNames);

	//getchar();
	return 0;
}

//definitions
void setSearchedExpression(int *p_argIndex, int argc, char* argv[]){
	if( ((*p_argIndex) = isArgument("-e",argc,argv))<0 ){
		searched_expression = DEFAULT_EXPRESSION;
		printf("... search initialized for default expression : \'%s\'\n",searched_expression);
	}else{
		searched_expression = argv[(*p_argIndex)+1];
		printf("... search initialized for user defined expression : \'%s\'\n",searched_expression);
	}
}

int parse_file_names(int indexOfF, char *argv[], char *fileNames[]){

	return split_string_according_separator(',', argv[indexOfF+1], FILES_COUNT, fileNames);
}

int parse_dir_names(int indexOfF, char *argv[], char *dirNames[]){

	return split_string_according_separator(',', argv[indexOfF+1], DIRS_COUNT, dirNames);
}

void handle_files_once_only(int filesRealCount,char *fileNames[]){
	
	FILE *f = NULL;
	int i;
	for( i=0 ; i< filesRealCount ; i++){
		//open file
		f = fopen(fileNames[i],"r");
		if(f == NULL){
			printf("File %s could be not open...skipping \n", fileNames[i]);
			continue;
		}

		//handle file
		find_expression_once_only(fileNames[i], f);

		if(fclose(f)!=0){
			fprintf(stderr,"File %s could not be closed properly...ignored \n", fileNames[i]);
		}
	}
}

void handle_files_recursively(int filesRealCount,char *fileNames[]){

	FILE *f = NULL;
	int i;
	for( i=0 ; i< filesRealCount ; i++){
		//open file
		f = fopen(fileNames[i],"r");
		if(f == NULL){
			printf("File %s could be not open...skipping \n", fileNames[i]);
			continue;
		}

		//handle file
		find_expression_recursively(fileNames[i], f);

		//close file
		if(fclose(f)!=0){
			fprintf(stderr,"File %s could not be closed properly...ignored \n", fileNames[i]);
		}
	}
}

void find_expression_once_only(char * name, FILE * fr){

	char singleLine[LINE_SIZE];
	int lineNumber = 1;

	printf("********************  %s  ********************\n",name);
	while ( fgets ( singleLine, LINE_SIZE, fr) != NULL ){
		//try to find an expression
		if(mystristr( singleLine, searched_expression)!=NULL){
			printf("Line: %d\n Match found: \n  %s",  lineNumber, singleLine);
			//printf(separator);
			return;
		}
		lineNumber++;
	}
}

void find_expression_recursively(char * name, FILE * fr){


	char singleLine[LINE_SIZE];
	int lineNumber = 1;

	printf("********************  %s  ********************\n",name);
	while ( fgets ( singleLine, LINE_SIZE, fr) != NULL ){
		//try to find an expression
		if(mystristr( singleLine, searched_expression)!=NULL){
			printf("Line: %d\n Match found: \n  %s",  lineNumber, singleLine);
			//printf(separator);
		}
		lineNumber++;
	}
}
