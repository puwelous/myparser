#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#include "myDirHandler.h"

/** 
	scans through the directory dir and since index index fullfils array fileNames with file names
	returns index of fileNames (real size)
*/
int get_directory_file_names(char *dir,int filesRealCount, char *fileNames[])
{
	struct stat stbuf;
	DIR *dfd;
	struct dirent *ent;
	char *file_name = NULL;
	char *dir_name = NULL;
	char prefix[NAME_OF_FILE_SIZE];
	memset(prefix,'\0',NAME_OF_FILE_SIZE);

	if (stat(dir, &stbuf) == -1)
	{
		fprintf(stderr, "Can't access %s\n", dir);
		return filesRealCount;
	}

	if ((stbuf.st_mode & S_IFMT) != S_IFDIR)
	{
		fprintf(stderr, "%s might be not a directory\n", dir);
		return filesRealCount;
	}

	if ((dfd = opendir(dir)) == NULL) {
		fprintf(stderr, "Can't open %s\n", dir);
		return filesRealCount;
	}

	/** prepare prefix */
	strcpy(prefix , dir);
	strcat(prefix , "\\");

	/* scanning through the directory */
	while ((ent = readdir (dfd)) != NULL) {

		switch (ent->d_type) {
		case DT_REG:

			file_name = (char *) malloc( (sizeof(char) * NAME_OF_FILE_SIZE));
			if(file_name == NULL){
				printf("Could not allocate enough memory for storing file names\n");
				return filesRealCount;
			}
			file_name = strcpy(file_name , dir);	// dir
			file_name = strcat(file_name , "\\");	// dir/
			file_name = strcat(file_name , ent->d_name);  //dir/file.txt
			fileNames[filesRealCount] = file_name;
			file_name = NULL;
			filesRealCount++;
			break;

		case DT_DIR:
			if( strcmp(ent->d_name,".")==0 || strcmp(ent->d_name,"..")==0)
			{
				printf("Skipping directory \'%s\'\n", ent->d_name);
				break;
			}
			dir_name = (char *) malloc( (sizeof(char) * NAME_OF_FILE_SIZE));
			if(dir_name == NULL)
			{
				printf("Could not allocate enough memory for storing a directory name\n");
				return filesRealCount;
			}
			dir_name = strcpy(dir_name , dir);
			dir_name = strcat(dir_name , "\\");
			dir_name = strcat(dir_name , ent->d_name);
			printf("Directory \'%s\'\n", dir_name);
			filesRealCount = get_directory_file_names(dir_name,filesRealCount, fileNames);
			break;

		default:
			printf("%s: (default)\n", ent->d_name);
		}
	}

	if(closedir(dfd)<0)
		printf("Dir %s not closed succesfully !\n", dir);

	return (filesRealCount);
}