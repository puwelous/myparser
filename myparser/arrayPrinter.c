#include <stdio.h>
#include "arrayPrinter.h"

/** prints prefix on the first line and then prints size elements of an array */
void printPrefixedArray(char *prePrint,int sizeOfArray, char *array1d[]){
	
	int i;
	printf("%s", prePrint);
	for( i=0 ; i<sizeOfArray ; i++){
		printf("\t-%s \n", array1d[i]);
	}
	putchar('\n');
}