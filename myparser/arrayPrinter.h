#ifndef ARRAYPRINTER_H_
#define ARRAYPRINTER_H_

/** prints prefix on the first line and then prints size elements of an array */
void printPrefixedArray(char *prePrint,int sizeOfArray, char *array[]);

#endif