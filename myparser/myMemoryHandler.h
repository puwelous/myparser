#ifndef MYMEMORYHANDLER_H_
#define MYMEMORYHANDLER_H_

/** frees memory calling funcition free() for each element array */
void free1DArray(int sizeOfArray, char *array[]);

/** nulls all pointers of each element of array */
void null1DArray(int sizeOfArray, char *array[]);

#endif
 