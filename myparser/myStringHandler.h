#ifndef MYSTRINGHANDLER_H_
#define MYSTRINGHANDLER_H_

/** checks case-insensitively whether string "needle" appears in "haystack" string */
const char *mystristr(const char *haystack, const char *needle);

/** fullfils array of pointers on char by indexes of separator in handledString*/
int split_string_according_separator(const char separator, char *handledString, const int suggestedFilesCount, char *fileNames[]);
#endif