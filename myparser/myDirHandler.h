#ifndef MYDIRHANDLER_H_
#define MYDIRHANDLER_H_

#define NAME_OF_FILE_SIZE 512

/** 
	scans through the directory dir and since index index fullfils array fileNames with file names
	returns index of fileNames (real size)
*/
int get_directory_file_names(char *dir,int index, char *fileNames[]);


#endif