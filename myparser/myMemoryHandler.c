#include <stdlib.h>
#include "myMemoryHandler.h"

/** frees memory calling funcition free() for each element array */
void free1DArray(int sizeOfArray, char *array1d[]){
	
	int i;
	
	for( i=0 ; i<sizeOfArray ; i++){
		free(array1d[i]);
		array1d[i] = NULL;
	}

}

/** nulls all pointers of each element of array */
void null1DArray(int sizeOfArray, char *array1d[]){

	int i;
	
	for( i=0 ; i<sizeOfArray ; i++){
		array1d[i] = NULL;
	}
}