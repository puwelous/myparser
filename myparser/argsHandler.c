#include <stdio.h>
#include <string.h>
#include "myStringHandler.h"

/** prints argc arguments contained in argv[] */
void printArgs(int argc, char *argv[]){

	int i;
	for( i=0; i<argc ; i++){
		printf(" %s",argv[i]);
	}
	putchar('\n');
}

/** checks whether argument argument can be found in argv[] */
int isArgument(const char *argument, int argc, char *argv[]){

	int i;
	for( i=1; i<argc ; i++){
		if( strcmp( argv[i], argument)==0 )
			return i;
	}
	return 0;
}

/** next 4 functions check whether user entered enough/not too many arguments */
int checkArgsCountMinInc(int argc, int min){
	return ( min <= argc );
}

int checkArgsCountMinExc(int argc, int min){
	return ( min < argc );
}

int checkArgsCountMaxInc(int argc, int max){
	return ( max >= argc );
}

int checkArgsCountMaxExc(int argc, int max){
	return ( max < argc );
}