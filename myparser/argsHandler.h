#ifndef ARGSHANDLER_H_
#define ARGSHANDLER_H_

/** prints all arguments */
void printArgs(int argc, char *argv[]);

/** checks whether argument argument is contained in args[] */
int isArgument(const char * argument, int argc, char *argv[]);

/** next 4 functions check whether user entered enough/not too many arguments */
int checkArgsCountMinInc(int argc, int min);
int checkArgsCountMinExc(int argc, int min);
int checkArgsCountMaxInc(int argc, int max);
int checkArgsCountMaxExc(int argc, int max);

#endif