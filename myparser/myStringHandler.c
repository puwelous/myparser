#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "myStringHandler.h"

const char *mystristr(const char *haystack, const char *needle)
{
	if ( !*needle )
	{
		return haystack;
	}
	for ( ; *haystack; ++haystack )
	{
		if ( toupper(*haystack) == toupper(*needle) )
		{
			const char *h, *n;
			for ( h = haystack, n = needle; *h && *n; ++h, ++n )
			{
				if ( toupper(*h) != toupper(*n) )
				{
					break;
				}
			}
			if ( !*n )
			{
				return haystack;
			}
		}
	}
	return 0;
}

// (',', "file1.txt,file2.txt,file3.txt", 512, char * poleSmernikovNaNazvySuborov[])
int split_string_according_separator(const char separator, char *handledString, const int suggestedFilesCount, char *fileNames[])
{

	char *string = handledString;
	int i = 0;
	int filesRealCount = 1; //automaticly relying that first file is really defined
	const int aaa = (const int) suggestedFilesCount;
	const int lengthOfHandledString = strlen(string);
	unsigned int pointerToArray=0;
	
	//int separatorIndexes[suggestedFilesCount];
	int *separatorIndexes = (int *) malloc(sizeof(int) * suggestedFilesCount);
	if( (void *) separatorIndexes == NULL)
		return -1;
	separatorIndexes[0] = -1; //pointer to name of first file: first.txt,second.txt,third.txt 

	//printf("Files argument: %s\n", files);
	while(i < lengthOfHandledString)
	{
		if(string[i] == separator)
		{
			//saving index of ',' into an indexes array
			separatorIndexes[filesRealCount] = i;
			filesRealCount++;
			string[i]='\0';
		}
		i++;
	}

	//copying pointers
	for( i=0 ; i < filesRealCount ; i++){

		fileNames[pointerToArray] = &string[separatorIndexes[i]+1];
		pointerToArray++;
	}

	string = NULL;
	return filesRealCount;
}